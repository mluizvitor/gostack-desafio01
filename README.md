# Go Stack - Desafio 01

![Language](https://img.shields.io/badge/DEV-Luiz%20Vitor%20Monteiro-E10F36?style=for-the-badge&labelColor=212121) ![Dev](https://img.shields.io/badge/LANG-JavaScript-FEC63F?style=for-the-badge&labelColor=212121)

Este repositório contem o código de resolução do `Desafio 01` do curso GoStack da Rocketseat.
Nele foi feito uma pequena aplicação de listas, contendo um **ID**, um **Título** e um vetor de **Tarefas**.

Foram criadas 5 rotas:

- `GET /projects` Para listar todos os projetos, seus **Títulos**, **IDs** e **Tarefas**;
- `POST /projects` Para criar um projeto com **ID** e **Título**;
- `POST /projects/:id/tasks` Para criar uma **Tarefa** em determinado projeto;
- `PUT /projects/:id` Para atualizar o **Título** de um projeto;
- `DELETE /projects/:id` Para deletar um projeto.

E 2 middlewares:

- `checkIdExists` Para verificar se um Id está sendo usado;
- Para imprimir no console o número de requisições executadas na aplicação até seu estado atual.

Para instruções completas do desafio, [acesse a documentação](https://github.com/Rocketseat/bootcamp-gostack-desafio-01/blob/master/README.md).

## Testando

Para usá-lo é necessário o uso da aplicação `Insomnia` configurada para acessar as rotas.
O [arquivo de configuração](insomnia.json) pode ser usado para agilizar o processo de criação de requests para teste.

Instruções: Abra o `Insomnia` → Clique na seta ao lado do nome do **Workspace** → Vá a `Import/Export` → Toque em `Import Data` → depois em `From File` → selecione o arquivo `insomnia.json` → Após a importação está pronto para testar.

## Sobre

Este repositório está sob a [licença MIT](LICENSE).

Feito com 🤩 por Luiz Vitor Monteiro
