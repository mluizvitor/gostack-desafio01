const express = require('express');

const server = express();

server.use(express.json());


const projects = [
  {
    id: "1",
    title: "Projeto 01",
    tasks: ["tarefa 01", "tarefa 02"]
  }
];

// middleware para verificar se um id do projeto já existe
function checkIdExists(req, res, next) {
  const {id} = req.params;
  const project = projects.find(project => project.id === id);

  if(!project) {
    return res.status(400).json({error: "Project does not exists"})
  }

  req.project = project;

  return next();
}

//middleware para verificar quantas requisições foram feitas até o estado atual da aplicação
server.use((req, res, next) => {

  console.count(`Total de requisições`);

  return next();
});

// rota para exibição de projetos
server.get('/projects', (req, res) => {
  return res.json(projects);
});

// rota para criação de projetos
server.post('/projects', (req, res) => {
  const {id, title} = req.body;
  const project = projects.find(project => project.id === id);

  if(project) {
    return res.status(400).json({error: "Project already exists with the same ID"});
  }

  if(!id) {
    return res.json({error: "Id is required"});
  }
  if(!title) {
    return res.json({error: "Title is required"});
  }

  projects.push({"id": id, "title": title, "tasks": []});

  return res.json(projects);
});

// rota par atualizar apenas o título do projeto
server.put('/projects/:id', checkIdExists, (req, res) => {
  const {title} = req.body;
  
  if(!title) {
    return res.status(400).json({error: "Title is required"})
  }

  req.project.title = title;

  return res.json(req.project);
});

// rota para deletar um projeto
server.delete('/projects/:id', checkIdExists, (req, res) => {
  const index = projects.findIndex(project => project.id === req.params.id);

  projects.splice(index, 1);

  return res.send();
  
});

// rota para inserção de tarefas no projeto 
server.post('/projects/:id/tasks', checkIdExists, (req, res) => {
  const {title} = req.body;

  req.project.tasks.push(title);

  return res.json(req.project);

});

server.listen(3000);